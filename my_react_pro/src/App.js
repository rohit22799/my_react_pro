import React from 'react';
import { Switch, Route } from 'react-router-dom';

import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Navbar from './components/Navbar';
import Productlist from './components/Productlist';
import Home from './components/Home'
import ScrollProgress from './components/scroll-progress';
import Details from './components/Details';
import Default from './components/Default';
import Cart from './components/Cart';
import Modal from './components/Modal'
import Signup from './components/Signup'
import Signin from './components/Signin'

function App() {
  return (
    <React.Fragment>
      <Navbar/>
      <ScrollProgress/>
      <Switch>
        <Route exact path="/" component={Home}/>
        <Route path="/shop" component={Productlist}/>
        <Route path="/details" component={Details}/>
        <Route path="/cart" component={Cart}/>
        <Route path="/signup" component={Signup}/>
        <Route path="/signin" component={Signin}/>
        <Route  component={Default}/>
      </Switch>
    <Modal/> 
    
    </React.Fragment>
  );
}

export default App;
