import React, { Component } from 'react'
import {Link} from 'react-router-dom'
import styled from 'styled-components';
import {NewButtonContainer , ButtonContainer} from './Button'

export default class Home extends Component{
    render(){
        return(
            <React.Fragment>
                    <HomeWrapper>
                        <div className="overlay fade-in">
                            <h2 className="col-xs-1 text-center">
                                Welcome To The Double Dips Shop!
                            </h2>
                            <Link to="/shop" className="d-flex justify-content-center">
                                <NewButtonContainer>
                                    Start Shopping
                                </NewButtonContainer>
                            </Link>
                        </div>
                        <div className="overlay_second fade-in">
                            <h2 className="col-xs-1 text-center">Already Done Shopping??</h2>
                            <Link to="/cart" className="d-flex justify-content-center">
                                <ButtonContainer className="button_div">
                                    Go To Cart
                                </ButtonContainer>
                            </Link>
                        </div>
                        <div className="overlay fade-in">
                        <h2 className="col-xs-1 text-center">
                            Want To Join Double Dips??
                            </h2>
                        
                        <Link to="/signup" className="d-flex justify-content-center">
                                <NewButtonContainer>
                                    Sign Up
                                </NewButtonContainer>
                            </Link>
                        </div>
                        <div className="overlay_second fade-in">
                            <h2 className="col-xs-1 text-center">Already User??</h2>
                            <Link to="/signin" className="d-flex justify-content-center">
                                <ButtonContainer className="button_div">
                                    Sign In!
                                </ButtonContainer>
                            </Link>
                        </div>
                    </HomeWrapper>
            </React.Fragment>
        );
    }
}
const HomeWrapper =  styled.div`
.overlay{
    background: #fff;
    position: relative;
    height:500px;
    width:100%;
    padding-top:220px;
}
.overlay_second{
    background:black;
    color:white;
    height:500px;
    position:relative;
    width:100%;
    padding-top:220px;
}
.button_div{
    border:1px solid white;
    color:blacck;
    background-coloe:white;
}
.fade-in {
    animation: fadeIn ease 2s;
    -webkit-animation: fadeIn ease 2s;
    -moz-animation: fadeIn ease 2s;
    -o-animation: fadeIn ease 2s;
    -ms-animation: fadeIn ease 2s;
    }
    @keyframes fadeIn {
    0% {opacity:0;}
    100% {opacity:1;}
    }
    
    @-moz-keyframes fadeIn {
    0% {opacity:0;}
    100% {opacity:1;}
    }
    
    @-webkit-keyframes fadeIn {
    0% {opacity:0;}
    100% {opacity:1;}
    }
    
    @-o-keyframes fadeIn {
    0% {opacity:0;}
    100% {opacity:1;}
    }
    
    @-ms-keyframes fadeIn {
    0% {opacity:0;}
    100% {opacity:1;}
    }
`;