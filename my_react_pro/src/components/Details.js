import React, { Component } from 'react'
import {ProductConsumer} from '../Context'
import {Link} from 'react-router-dom'
import {NewButtonContainer} from './Button'
export default class Details extends Component {
    render() {
        return (
           <ProductConsumer>
               {value=>{
                   const {id,company,img,info,price,title,inCart}
                   =value.detailProduct
                   return(
                       <div className="container py-5">
                           <div className="row">
                               <div className="col-10 mx-auto text-center 
                               text-slanted text-blue my-5">
                                <h1>{title}</h1>
                               </div>
                           </div>
                           
                           <div className="row">
                           <div className="col-10 mx-auto col-md-6 mt-3">
                           <img src={img} className="img-fluid" alt="product"/>
                           </div>
                           <div className="col-10 mx-auto col-md-6 mt-3 text-capitalize">
                               
                                <h4>Model : {title}</h4>
                                <h4 className="text-title mt-3 mb-2">
                                    made by : <span>{company}</span>
                                </h4>
                                <h4 className="text-blue">
                                    <span>price : <span>$</span>{price}
                                    </span>
                                </h4>
                                <p className="text-capitalize mt-3 mb-3">
                                    Some info of the product :{info}
                                </p>
                                <div>
                                    <Link to="/shop">
                                        <NewButtonContainer>
                                            back to products
                                        </NewButtonContainer>
                                    </Link>
                                    <NewButtonContainer cart disabled={inCart?true:false} onClick={()=>{
                                        value.addToCart(id);
                                        value.openModal(id);

                                    }}>
                                        {inCart?"inCart":"add to cart"}
                                    </NewButtonContainer>
                                </div>
                               </div>
                           </div>
                       </div>
                   )
               }}
           </ProductConsumer>
        )
    }
}