import React, { Component } from 'react'
import {Link} from 'react-router-dom';
import {ButtonContainer} from './Button';
import styled from 'styled-components';
export default class Navbar extends Component {
    render() {
        return (
            <NavWrapper className="navbar navbar-expand-sm  navbar-dark px-sm-5 ">
                <ul className="navbar-nav align-items center ">
                    <li className="nav-item ml-5 cart_class">
                        <Link to="/" className="nav-link">home</Link>
                    </li>
                    <li className="nav-item ml-5 cart_class">
                        <Link to="/shop" className="nav-link">shop</Link>
                    </li>
                </ul>
                <Link to="/cart" className="ml-auto">
                    <ButtonContainer>
                        <span className="mr-2">
                        <i className="fa fa-shopping-cart cart_class" aria-hidden="true"/>
                        </span>my cart</ButtonContainer >
                </Link>
                <Link to="/signup">
                    <ButtonContainer>
                        <span className="mr-2">
                            <i className="fa fa-user" aria-hidden="true"/>
                        </span>sign up
                    </ButtonContainer>
                </Link>
            </NavWrapper>
        )
    }
}

const NavWrapper = styled.nav`
background: black;
.nav-link{
    color:var(--mainWhite)!important;
    font-size:1.3rem;
    text-transform:capitalize;
}
.cart_class{
    color:white;
}

`;