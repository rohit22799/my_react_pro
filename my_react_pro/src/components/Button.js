import styled from 'styled-components';
export const ButtonContainer = styled.button`
text-transform:capitalize;
font-size:1.2rem;
background : transparent;
border:none;
color:${prop => prop.cart?"var(--mainYellow)":"white"};
border-radius: 0.3rem;
padding:0.2rem 0.5rem;
cursor:pointer;
margin:0.2rem 0.5rem;
transition:all 0.5s ease-in-out;
&:hover{
    background:${prop => prop.cart?"var(--mainYellow)":"black"};
    color:white;
}
&:focus{
outline: none;
}
`;
export const NewButtonContainer = styled.button`
text-transform:capitalize;
font-size:1.2rem;
background : black;
border:none;
color:${prop => prop.cart?"white":"white"};
border-radius: 0.3rem;
padding:0.2rem 0.5rem;
cursor:pointer;
margin:0.2rem 0.5rem;
transition:all 0.5s ease-in-out;
&:hover{
    background:${prop => prop.cart?"black":"black"};
    color:white;
}
&:focus{
outline: none;
}
`;
//export ButtonContainer;